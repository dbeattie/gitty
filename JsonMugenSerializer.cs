﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters;
using System.Text;
using MugenMvvmToolkit;
using MugenMvvmToolkit.Collections;
using MugenMvvmToolkit.Interfaces;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace EnjinMobile.Core.Helpers
{
    public class JsonMugenSerializer : ISerializer
    {
        #region Nested types

        private sealed class MugenContractResolver : DefaultContractResolver
        {
            #region Fields

            private static readonly Dictionary<Type, bool> IsLightDictionaryCache;

            #endregion

            #region Constructors

            static MugenContractResolver()
            {
                IsLightDictionaryCache = new Dictionary<Type, bool>();
            }

            #endregion

            #region Methods

            private static bool IsLightDictionaryBase(Type objectType)
            {
                while (objectType != null && objectType != typeof(object))
                {
                    var typeInfo = objectType.GetTypeInfo();
                    if (typeInfo.IsGenericType && objectType.GetGenericTypeDefinition() == typeof(LightDictionaryBase<,>))
                        return true;
                    objectType = typeInfo.BaseType;
                }
                return false;
            }

            protected override JsonContract CreateContract(Type objectType)
            {
                bool value;
                lock (IsLightDictionaryCache)
                {
                    if (!IsLightDictionaryCache.TryGetValue(objectType, out value))
                    {
                        value = IsLightDictionaryBase(objectType);
                        IsLightDictionaryCache[objectType] = value;
                    }
                }
                if (value)
                    return CreateObjectContract(objectType);
                return base.CreateContract(objectType);
            }

            #endregion
        }

        #endregion

        #region Fields

        private static readonly JsonSerializerSettings SerializerSettings;

        #endregion

        #region Constructors

        static JsonMugenSerializer()
        {
            SerializerSettings = new JsonSerializerSettings
            {
                TypeNameAssemblyFormat = FormatterAssemblyStyle.Simple,
                TypeNameHandling = TypeNameHandling.All,
                ConstructorHandling = ConstructorHandling.AllowNonPublicDefaultConstructor,
                PreserveReferencesHandling = PreserveReferencesHandling.Objects,
                ReferenceLoopHandling = ReferenceLoopHandling.Serialize,
                ContractResolver = new MugenContractResolver()
            };
        }

        #endregion

        #region Implementation of interfaces

        public Stream Serialize(object item)
        {
            var s = JsonConvert.SerializeObject(item, Formatting.None, SerializerSettings);
            var stream = new MemoryStream(Encoding.UTF8.GetBytes(s));
            return stream;
        }

        public object Deserialize(Stream stream)
        {
            var array = stream.ToArray();
            var s = Encoding.UTF8.GetString(array, 0, array.Length);
            var item = JsonConvert.DeserializeObject<object>(s, SerializerSettings);
            return item;
        }

        public bool IsSerializable(Type type)
        {
            var typeInfo = type.GetTypeInfo();
            return type == typeof(string) || typeInfo.IsDefined(typeof(DataContractAttribute)) || typeInfo.IsPrimitive ||
                   typeInfo.IsDefined(typeof(JsonObjectAttribute));
        }

        #endregion
    }
}
