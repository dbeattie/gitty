using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MugenMvvmToolkit;
using Serilog;

namespace EnjinMobile.Core.Helpers
{
    public class LoggingHandler : DelegatingHandler
    {
        public LoggingHandler(HttpMessageHandler innerHandler)
            : base(innerHandler)
        {
        }

        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request,
                                                                     CancellationToken cancellationToken)
        {
            var sb = new StringBuilder();
            sb.AppendLine("Request:");
            sb.AppendLine(request.ToString());
            if (request.Content != null)
                sb.AppendLine(await request.Content.ReadAsStringAsync());
            Tracer.Info(sb.ToString());
            Log.Information(sb.ToString());

            var response = await base.SendAsync(request, cancellationToken);

            sb = new StringBuilder();
            sb.AppendLine("Response:");
            sb.AppendLine(response.ToString());

            if (response.Content != null)
                sb.AppendLine(await response.Content.ReadAsStringAsync());
            Tracer.Info(sb.ToString());
            Log.Information(sb.ToString());

            return response;
        }
    }
}